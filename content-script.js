// Copy the selected text to clipboard

function copySelection() {
  var selectedText = window.getSelection().toString().trim();

  console.log(typeof selectedText)
  console.log("Selected text: ", selectedText);
  
  function oncopy(event) {
    document.removeEventListener("copy", oncopy, true);
    // Hide the event from the page to prevent tampering.
    event.stopImmediatePropagation();

    // Overwrite the clipboard content.
    event.preventDefault();
    // Change format of clipboard data to plain text
    event.clipboardData.setData("text/plain", selectedText);
  }
  document.addEventListener("copy", oncopy, true)
  document.execCommand("Copy");
}

//Add copySelection() as a listener to mouseup events.
document.addEventListener("mouseup", copySelection);